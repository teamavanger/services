package com.reward.dao.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Category {
int category_id;
int title;
int status;
Date publish_date;
int merchant_id;


@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
@JoinColumn(name = "category_id")
private Reward reward;




public int getCategory_id() {
	return category_id;
}
public void setCategory_id(int category_id) {
	this.category_id = category_id;
}
public int getTitle() {
	return title;
}
public void setTitle(int title) {
	this.title = title;
}
public int getStatus() {
	return status;
}
public void setStatus(int status) {
	this.status = status;
}
public Date getPublish_date() {
	return publish_date;
}
public void setPublish_date(Date publish_date) {
	this.publish_date = publish_date;
}
public int getMerchant_id() {
	return merchant_id;
}
public void setMerchant_id(int merchant_id) {
	this.merchant_id = merchant_id;
}

}
